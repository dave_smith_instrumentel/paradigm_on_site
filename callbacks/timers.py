# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

# Built in modules
import datetime
import logging
import socket

# Third party modules
import dash
from dash.dependencies import Input, Output, State
import pandas as pd
import plotly
import plotly.graph_objs as go
import plotly.subplots as sp
import plotly.express as px
import paho.mqtt.client as mqtt
import os

# Custom modules
from hub import di_hub
import hub_setup
import udp_handler as udp
import tcp_handler as tcp
import graphing

import Internal_NetworkTools.processing.universal_processing as universal_processing


if not os.path.isfile("data\live.csv"):
    _live_df = None

else:
    _live_df = pd.read_csv("data\live.csv")

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    """

    :param client:
    :param userdata:
    :param flags:
    :param rc:
    :return:
    """
    global di_hub

    print("Connected with result code "+str(rc))
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("Unassigned")
    print(userdata)

    client.user_data_set({'db_conf': None})

    # Set the IP address
    if di_hub.online:
        _msg = hub_setup.build_message("action,db_conf")
        _reply = tcp.send_bytearray(di_hub, _msg)
        print(_reply)
        client.user_data_set({'db_conf': _reply})

    #userdata =

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    global mqtt_subs
    global _live_df

    if userdata['db_conf'] is None:
        # Set the IP address
        if di_hub.online:
            _msg = hub_setup.build_message("action,db_conf")
            _reply = tcp.send_bytearray(di_hub, _msg)
            print(_reply)
            client.user_data_set({'db_conf': _reply})

    #print(msg.topic + " " + str(msg.payload))
    #print("on_message")

    try:
        _df = universal_processing.process_liveview_data(msg.payload, userdata['db_conf'])
        #print("DT before: {}".format(_df['Timestamp'].iloc[0]))
        _df['Timestamp'] = pd.to_datetime(_df['Timestamp'], utc=True, unit='s')
        #print("DT after: {}".format(_df['Timestamp'].iloc[0]))

        if _live_df is not None:
            _live_df = pd.concat([_live_df, _df], ignore_index=True, sort=True)

            if len(_live_df) > 300:
                _live_df = _live_df[-300:]

        else:
            _live_df = _df

        _live_df.to_csv("data\live.csv")
        #print("Dataframe len: {}".format(len(_live_df)))
        #mqtt_subs += str(msg.payload) + "\n"
        #print("on_message, Len of MQTT data: {}".format(len(mqtt_subs)))

    except Exception as e:
        print(e)
    # Parse the payload and process it

client = mqtt.Client()
client._client_id = "python_client_id"
client.on_connect = on_connect
client.on_message = on_message
client.connect("127.16.0.98", 1883, 30)

print("looping")
client.loop() # Blocks for 100ms




# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Dash Layout
# %% Must be called before the callback functions as the decorators need the layout to determine the inputs


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Classes




# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Functions

def callback_interval_debug(app):
    _local_ip = socket.gethostbyname(socket.gethostname())
    _udp_socket = udp.udp_socket(_local_ip)

    @app.callback([Output('textarea-debug', 'children')],
                  [Input('interval-debug', 'n_intervals'), Input('radio-debug', 'value')],
                  [State('textarea-debug', 'children')])
    def timer_debug(n, a_on_off, a_textarea):
        """
        :Desc: This function will parse the input data from the form input and validate they meet the conditions. If so then
               it will program the hub and inform the user of success or failure via a pop up box. If the user input invalid
               information to program, then the pop up box will inform accordingly!
        :param n:
        :return:
        """
        global di_hub

        _local_ip = socket.gethostbyname(socket.gethostname())
        # Check if the available local connection has changed
        if _local_ip != _udp_socket.local_ip_addr:
            _udp_socket.update(_local_ip)

        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

        if 'radio-debug' in changed_id:
            # Someone has pressed a radio button
            if a_on_off == "debug_on":
                _str = "debug,on=1"

            elif a_on_off == "debug_off":
                _str = "debug,off=1"

            # Set the IP address
            _msg = hub_setup.build_message(_str)
            _reply = tcp.send_bytearray(di_hub, _msg)

        _recv = _udp_socket.receive()

        for _byte_data in _recv:
            try:
                a_textarea = "{}: ".format(datetime.datetime.utcnow()) + _byte_data.decode("utf-8") + a_textarea

            except Exception as e:
                print(e)
                continue
        #_reply = udp.udp_listen(socket.gethostbyname(socket.gethostname()), 46787)

        #a_textarea += _recv

        return [a_textarea]

def callback_interval_hub_scan(app):

    _local_ip = socket.gethostbyname(socket.gethostname())
    _udp_socket = udp.udp_socket(_local_ip)

    @app.callback([Output('input-hub-ip', 'value'), Output('input-hub-sn', 'value'), Output('input-hub-gw', 'value'),
                   Output('input-hub-serial', 'value'), Output('input-hub-loc', 'value'), Output('radio-logging-file-type', 'value'),
                   Output('radio-logging-gps', 'value'), Output('input-gps-port', 'value'), Output('input-gps-preamble', 'value'),
                   Output('input-mqtt-ip', 'value'), Output('input-mqtt-port', 'value'), Output('input-mqtt-topic', 'value'),
                   Output('input-mqtt-qos', 'value'), Output('interval-discover', 'disabled')],
                  [Input('interval-discover', 'n_intervals')])
    def update_hub_details(n):
        """
        :Desc: This function will parse the input data from the form input and validate they meet the conditions. If so then
               it will program the hub and inform the user of success or failure via a pop up box. If the user input invalid
               information to program, then the pop up box will inform accordingly!
        :param n:
        :return:
        """
        global di_hub

        _gps_port = ''
        _gps_preamble = ''
        _mqtt_addr = ''
        _mqtt_port = ''
        _mqtt_topic = ''
        _mqtt_qos = ''

        _local_ip = socket.gethostbyname(socket.gethostname())
        # Check if the available local connection has changed
        if _local_ip != _udp_socket.local_ip_addr:
            _udp_socket.update(_local_ip)

        try:
            _same_network = hub_setup.discover_hub(_udp_socket, di_hub)
        except Exception as e:
            print(e)

        if di_hub.online:

            # We've found a hub and it's on our network so get some more info
            if _same_network:
                # Set the IP address
                _msg = hub_setup.build_message("{},{}=1".format('action', "gps_port"))
                _gps_port = tcp.send_bytearray(di_hub, _msg)

                if _gps_port is not None:
                    print("Rx: ", _gps_port)
                    _gps_port = str(_gps_port).split("=")[1][:-1]

                _msg = hub_setup.build_message("{},{}=1".format('action', "gps_preamble"))
                _gps_preamble = tcp.send_bytearray(di_hub, _msg)

                if _gps_preamble is not None:
                    print("Rx: ", _gps_preamble)
                    _gps_preamble = str(_gps_preamble).split("=")[1][:-1]

                _msg = hub_setup.build_message("{},{}=1".format('action', "mqtt_addr"))
                _mqtt_addr = tcp.send_bytearray(di_hub, _msg)

                if _mqtt_addr is not None:
                    print("Rx: ", _mqtt_addr)
                    _mqtt_addr = str(_mqtt_addr).split("=")[1][:-1]

                _msg = hub_setup.build_message("{},{}=1".format('action', "mqtt_port"))
                _mqtt_port = tcp.send_bytearray(di_hub, _msg)

                if _mqtt_port is not None:
                    print("Rx: ", _mqtt_port)
                    _mqtt_port = str(_mqtt_port).split("=")[1][:-1]

                _msg = hub_setup.build_message("{},{}=1".format('action', "mqtt_topic"))
                _mqtt_topic = tcp.send_bytearray(di_hub, _msg)

                if _mqtt_topic is not None:
                    print("Rx: ", _mqtt_topic)
                    _mqtt_topic = str(_mqtt_topic).split("=")[1][:-1]

                _msg = hub_setup.build_message("{},{}=1".format('action', "mqtt_qos"))
                _mqtt_qos = tcp.send_bytearray(di_hub, _msg)

                if _mqtt_qos is not None:
                    print("Rx: ", _mqtt_qos)
                    _mqtt_qos = str(_mqtt_qos).split("=")[1][:-1]

            if di_hub.bit_flags & 0x10:
                _file_type = '5'
            else:
                _file_type = '60'

            #if bit_flags & 0x40:
            #    self.safe_mode = True

            if di_hub.bit_flags & 0x100:
                _gps = 'gps_parse'
                # print("GPS Set to parse.")
            else:
                _gps = 'gps_log'
                # print("GPS Set to log.")


            print("Hub is online!".format(di_hub))
            _str = [di_hub.ip_str, di_hub.sn_str, di_hub.gw_str, di_hub.serial, di_hub.location, _file_type, _gps,
                    _gps_port,
                    _gps_preamble,
                    _mqtt_addr,
                    _mqtt_port,
                    _mqtt_topic,
                    _mqtt_qos,
                    True]

        else:

            _str = ["", "", "", "", "", '60', "gps_log", _gps_port       ,
_gps_preamble   ,
_mqtt_addr      ,
_mqtt_port      ,
_mqtt_topic     ,
_mqtt_qos       , False]

        return _str

def callback_interval_liveview(app):
    # Multiple components can update everytime interval gets fired.
    @app.callback([Output('live-update-graph', 'figure')],
                  [Input('interval-liveview', 'n_intervals')])
    def update_graph_live(n):
        global client
        global mqtt_subs

        #print("update_graph_live")

        client.loop()  # Blocks for 100ms

        #_df = pd.DataFrame(columns=['unixtime', 'signal'])

        if len(_live_df):
        #    We have data so build the figue.

            fig = graphing.scatter(_live_df, "SWR Test Hub", "Timestamp")

            #fig = px.line(_df, x='unixtime', y='signal')

        #print("update_graph_live, Len of MQTT data: {}".format(len(mqtt_subs)))
        return [fig]


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main

