# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

# Built in modules
import logging


# Third party modules
import dash
from dash.dependencies import Input, Output, State

# Custom modules
from hub import di_hub
import hub_setup
import verify_functions
import udp_handler as udp
import tcp_handler as tcp


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants

DEBUG_OPTS = {
    "DEBUG_GENERAL"		    :       0x00000001     ,
"DEBUG_UDP"  			:   	0x00000002     ,
"DEBUG_TCP"       		:		0x00000004     ,
"DEBUG_RTC"       		:		0x00000008     ,
"DEBUG_MICRO_SD"    	:		    0x00000010 ,
"DEBUG_FLASH"        	:		    0x00000020 ,
"DEBUG_SYS_CTRL"     	:		    0x00000040 ,
"DEBUG_HUB_CLIENT"   	:		    0x00000080 ,
"DEBUG_BOOTLOADER"   	:		    0x00000100 ,
"DEBUG_WATCHDOG"        :        0x00000200    ,
"DEBUG_RING_BUFFER"     :         0x00000400   ,
"DEBUG_LWIP"            :         0x00000800   ,
"DEBUG_MMC"             :         0x00001000   ,
"DEBUG_SYS"             :         0x00002000   ,
"DEBUG_CODEC"           :         0x00004000   ,
"DEBUG_GPS"             :         0x00008000   ,
"DEBUG_FW70"            :         0x00010000   ,
"DEBUG_RFID_DECODER"	:		    0x00020000 ,
"DEBUG_RFID_DDU_SM"		:		0x00040000     ,
"DEBUG_CALIB_DATA"		:		0x00080000     ,
"DEBUG_DIGITAL"			:	    0x00100000     ,
"DEBUG_PULSE" 			:       0x00200000     ,
"DEBUG_CONFIG_LOAD"		:       0x00400000     ,
"DEBUG_TRIGGERS"		:	        0x00800000 ,
"DEBUG_HS_CAPTURE"		:       0x01000000     ,
"DEBUG_ADC"   			:       0x02000000     ,
"DEBUG_BUFFMAN"			:       0x04000000     ,
"DEBUG_HW_LAYER"		:	        0x08000000 ,
"DEBUG_DRVR"  			:       0x10000000     ,
"DEBUG_APP"			    :       0x20000000     ,
"DEBUG_MQTT"  			:       0x40000000     ,
"DEBUG_BLANK10"			:       0x80000000     ,
"DEBUG_ALL"             :         0xFFFFFFFF
}


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Dash Layout
# %% Must be called before the callback functions as the decorators need the layout to determine the inputs


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Classes




# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Functions

def check_and_send(a_command_type, a_key, a_value, a_check_func, a_expected_reply, *a_check_func_args):
    global di_hub

    if a_check_func is not None:

        if not a_check_func(a_value, *a_check_func_args):
            return False, "Failed to verify {}: {}".format(a_key, a_value)

    # Set the IP address
    _msg = hub_setup.build_message("{},{}={}".format(a_command_type, a_key, a_value))
    _reply = tcp.send_bytearray(di_hub, _msg)

    if a_expected_reply is not None:

        if _reply.decode("utf-8") != a_expected_reply:
            print("Unexpected Reply.")
            return False, "Unexpected reply: {}".format(_reply.decode("utf-8"))

    return True, None


def callback_reprogram_hub_info(app):
    # Multiple components can update everytime interval gets fired.
    @app.callback([Output('modal-hub-info', 'is_open'), Output('modal-hub-info-header', 'children'), Output('modal-hub-info-body', 'children')],
                  [Input('button-update-hub-info', 'n_clicks'), Input("modal-hub-info-close", "n_clicks")],
                  [State('input-hub-ip', 'value'), State('input-hub-sn', 'value'), State('input-hub-gw', 'value'),
                   State('input-hub-serial', 'value'), State('input-hub-loc', 'value')])
    def reprogram_hub_info(a_clicks, a_close, a_ip, a_sn, a_gw, a_serial, a_loc):
        global di_hub

        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

        if 'modal-hub-info-close' in changed_id:
            print("Close button press.")
            return False, [], []

        elif 'button-update-hub-info' not in changed_id:
            print("Button pressed.")
            return False, [], []

        try:
            # Check the IP address
            _success, _err_message = check_and_send("change", "ip", a_ip, verify_functions.verify_ip_address, "NEW IP", [])

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            # Check the SN address
            _success, _err_message = check_and_send("change", "sn", a_sn, verify_functions.verify_ip_address, "NEW SN", [])

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            # Check the SN address
            _success, _err_message = check_and_send("change", "gw", a_gw, verify_functions.verify_ip_address, "NEW GW", [])

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            # Check the serial number
            _success, _err_message = check_and_send("change", "serial", a_serial, verify_functions.verify_string, "NEW SERIAL", [20])

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            # Check the location number
            _success, _err_message = check_and_send("change", "location", a_loc, verify_functions.verify_string, "NEW LOCATION", [40])

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            # Commit the changes
            _success, _err_message = check_and_send("change", "commit", "1", None, "COMMIT", None)

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

            """
            # Reboot the hub
            _success, _err_message = check_and_send("action", "reboot", "1", None, None, None)
    
            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]
                """

            di_hub.online = False

        except Exception as e:
            return True, ["Failed To Update Hub Info"], [str(e)]

        return True, ["Update Hub Info"], ["Successfully updated the hub!"]

def callback_reprogram_debug_info(app):
    # Multiple components can update everytime interval gets fired.
    @app.callback([Output('modal-hub-debug', 'is_open'), Output('modal-hub-debug-header', 'children'), Output('modal-hub-debug-body', 'children')],
                  [Input('button-update-debug-info', 'n_clicks'), Input("modal-hub-debug-close", "n_clicks")],
                  [State('input-debug-ip', 'value'), State('input-debug-port', 'value'), State('select-debug-on', 'children')])
    def reprogram_debug_info(a_clicks, a_close, a_ip, a_port, a_debug_sources):
        global hub

        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]
        print(changed_id, a_clicks, a_close, a_ip, a_port)

        if 'modal-hub-debug-close' in changed_id:
            print("Close button press.")
            return False, [], []

        elif 'button-update-debug-info' not in changed_id:
            print("Update button not pressed.")
            return False, [], []

        try:

            # Check the IP address
            _success, _err_message = check_and_send("change", "udp_ip", a_ip, verify_functions.verify_ip_address, "NEW UDP IP", [])

            if not _success:
                return True, ["Failed To Update Debug Info"], [_err_message]

            # Check and send the UDP port number
            _success, _err_message = check_and_send("change", "udp_port", a_port, verify_functions.verify_port_number, "NEW UDP PORT", [])

            if not _success:
                return True, ["Failed To Update Debug Info"], [_err_message]

            _debug_sources = 0x01

            for _opt in a_debug_sources:
                _debug_sources |= DEBUG_OPTS[_opt['props']['children']]

            # Check and send the UDP port number
            print("Debug sources: {}".format(_debug_sources))
            _success, _err_message = check_and_send("debug", "sources", str(_debug_sources), None, "UPDATE SOURCES", [])

            if not _success:
                return True, ["Failed To Update Debug Info"], [_err_message]

            # Commit the changes
            _success, _err_message = check_and_send("change", "commit", "1", None, "COMMIT", None)

            if not _success:
                return True, ["Failed To Update Hub Info"], [_err_message]

        except Exception as e:
            return True, ["Failed To Update Hub Info"], [str(e)]

        return True, ["Update Debug Info"], ["Successfully updated the hub!"]

def callback_update_debug_options(app):

    @app.callback([Output('select-debug-on', 'children'), Output('select-debug-off', 'children')],
                  [Input('button-debug-add', 'n_clicks'), Input('button-debug-minus', 'n_clicks')],
                  [State('select-debug-on', 'children'), State('select-debug-off', 'children')])
    def update_debug_options(add_clicks, minus_clicks, select_on, select_off):

        _to_be_removed = []

        changed_id = [p['prop_id'] for p in dash.callback_context.triggered][0]

        if 'button-debug-add' in changed_id:
            _list_to_add_to = select_on
            _list_to_remove_from = select_off

        elif 'button-debug-minus' in changed_id:
            _list_to_add_to = select_off
            _list_to_remove_from = select_on

        else:
            return [select_on, select_off]

        for _opt in _list_to_remove_from:
            pass
            if 'n_clicks' in _opt['props'].keys():

                if _opt['props']['n_clicks'] > 0:
                    _opt['props']['n_clicks'] = 0
                    print("Opt added to on: {}".format(_opt))
                    _list_to_add_to.append(_opt)
                    _to_be_removed.append(_opt)

        for _opt in _to_be_removed:
            print("Opt removed from off: {}".format(_opt['props']['children']))
            _list_to_remove_from.remove(_opt)


        return [select_on, select_off]

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main

