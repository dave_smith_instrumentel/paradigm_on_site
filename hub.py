# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

# Built in modules
import logging
import datetime
import pytz

# Third party modules


# Custom modules


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Dash Layout
# %% Must be called before the callback functions as the decorators need the layout to determine the inputs


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Classes

class Hub():

    def __init__(self):
        self.ip = []
        self.sn = []
        self.gw = []
        self.location = ''
        self.serial = ''
        self.online = False
        self.db_config_b64 = None
        self.db_config = None
        self.file_config = None
        self.tcp = None
        self.time = ''
        self.ip_str = None
        self.file_type = 60
        self.gps_port = 6060
        self.safe_mode = False
        self.bit_flags = None

        self.dirs = []
        self.files = []

    def __str__(self):
        ip_str = (str(self.ip[0]) + "." + str(self.ip[1]) + "." +
                  str(self.ip[2]) + "." + str(self.ip[3]))
        sn_str = (str(self.sn[0]) + "." + str(self.sn[1]) + "." +
                  str(self.sn[2]) + "." + str(self.sn[3]))
        gw_str = (str(self.gw[0]) + "." + str(self.gw[1]) + "." +
                  str(self.gw[2]) + "." + str(self.gw[3]))

        return ("""
Serial Number: {0}
Location: {1}
IP address: {2}
Subnet Mask: {3}
Gateway address: {4}
Time: {5}
GPS Port: {6}
File type: {7}
Safe mode: {8}""".format(self.serial, self.location, ip_str, sn_str, gw_str, self.time, self.gps_port, self.file_type,
                         self.safe_mode))

    def populate_hub_details(self, a_discover_reply):
        """

        :param a_discover_reply:
        :return:
        """
        print(type(a_discover_reply))
        a_discover_reply = a_discover_reply.split(",".encode('utf-8'))

        if a_discover_reply[0].decode("utf-8") != "discover":
            print("Could not find discover")
            return False

        for _part in a_discover_reply[1:]:

            _pair = _part.split("=".encode('utf-8'))
            _key = _pair[0].decode("utf-8")
            _value = _pair[1].decode("utf-8")

            if _key == "ip":
                hub_ip_addr = _value.split(".")
                self.ip = list(map(int, hub_ip_addr))
                self.ip_str = (str(self.ip[0]) + "." + str(self.ip[1]) + "." + str(self.ip[2]) + "." + str(self.ip[3]))

            if _key == "sn":
                hub_sn_addr = _value.split(".")
                self.sn = list(map(int, hub_sn_addr))
                self.sn_str = (str(self.sn[0]) + "." + str(self.sn[1]) + "." + str(self.sn[2]) + "." + str(self.sn[3]))

            if _key == "gw":
                hub_gw_addr = _value.split(".")
                self.gw = list(map(int, hub_gw_addr))
                self.gw_str = (str(self.gw[0]) + "." + str(self.gw[1]) + "." + str(self.gw[2]) + "." + str(self.gw[3]))

            if _key == "location":
                self.location = _value

            if _key == "serial":
                self.serial = _value

            if _key == "time":
                hub_dt = datetime.datetime.fromtimestamp(float(_value), tz=pytz.utc)
                self.time = hub_dt.strftime("%Y/%m/%d %H:%M:%S")

            if _key == "gps_port":
                self.gps_port = _value

            if _key == "bit_flags":
                self.bit_flags = int(_value, 16)

                if self.bit_flags & 0x10:
                    self.file_type = 5
                else:
                    self.file_type = 60

                if self.bit_flags & 0x40:
                    self.safe_mode = True
                    # print("SAFE MODE SET")
                else:
                    self.safe_mode = False

                if self.bit_flags & 0x100:
                    pass
                    # print("GPS Set to parse.")
                else:
                    pass
                    # print("GPS Set to log.")

        self.online = True

        return True

    def rx_data(self):
        pass

    def tx_data(self):
        pass


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Functions


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main

di_hub =  Hub()

