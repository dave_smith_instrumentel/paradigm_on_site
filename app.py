# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

import socket
import os
import sys

# Third party modules
import dash
import dash_core_components as dcc
import dash_bootstrap_components as dbc
import dash_html_components as html
import paho.mqtt.client as mqtt

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Set the environment paths

VERSION = "20.09.001"

INST_NETTOOLS = os.environ.get("INST_NETTOOLS", None)
print("INST_NETTOOLS = ", INST_NETTOOLS)

## Make sure the GPC paths have been set
if INST_NETTOOLS is None:
    print("env INST_NETTOOLS not set!")
    sys.exit(-1)

sys.path.append(INST_NETTOOLS)

INST_PYLIB = os.environ.get("INST_PYLIB", None)
print(INST_PYLIB)

## Make sure the GPC paths have been set
if INST_PYLIB is None:
    print("env INST_PYLIB not set!")
    sys.exit(-1)

sys.path.append(INST_PYLIB)

# Python imports to keep the golden tool tidy and clean
import hub as hub_import

# Python imports for FW0023

# Callbacks
from callbacks import timers
from callbacks import buttons

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants

LOCAL_IP_ADDR = socket.gethostbyname(socket.gethostname())

# We need to following folders to download, process and view data
WORKING_DIR = os.path.dirname(os.path.realpath(__file__))
LOG_DIR = os.path.join(WORKING_DIR, 'data', 'log')
RAW_DIR = os.path.join(WORKING_DIR, 'data', 'raw')
PROCESSED_DIR = os.path.join(WORKING_DIR, 'data', 'processed')
ANALYSIS_DIR = os.path.join(WORKING_DIR, 'data', 'analysis')
CONFS_DIR = os.path.join(WORKING_DIR, 'data', 'confs')
TEMP_DIR = os.path.join(WORKING_DIR, "data", "temp")
TEST_DIR = os.path.join(WORKING_DIR, 'data', 'test')

DEBUG_OPTS = {
    "DEBUG_GENERAL"		    :       0x00000001     ,
"DEBUG_UDP"  			:   	0x00000002     ,
"DEBUG_TCP"       		:		0x00000004     ,
"DEBUG_RTC"       		:		0x00000008     ,
"DEBUG_MICRO_SD"    	:		    0x00000010 ,
"DEBUG_FLASH"        	:		    0x00000020 ,
"DEBUG_SYS_CTRL"     	:		    0x00000040 ,
"DEBUG_HUB_CLIENT"   	:		    0x00000080 ,
"DEBUG_BOOTLOADER"   	:		    0x00000100 ,
"DEBUG_WATCHDOG"        :        0x00000200    ,
"DEBUG_RING_BUFFER"     :         0x00000400   ,
"DEBUG_LWIP"            :         0x00000800   ,
"DEBUG_MMC"             :         0x00001000   ,
"DEBUG_SYS"             :         0x00002000   ,
"DEBUG_CODEC"           :         0x00004000   ,
"DEBUG_GPS"             :         0x00008000   ,
"DEBUG_FW70"            :         0x00010000   ,
"DEBUG_RFID_DECODER"	:		    0x00020000 ,
"DEBUG_RFID_DDU_SM"		:		0x00040000     ,
"DEBUG_CALIB_DATA"		:		0x00080000     ,
"DEBUG_DIGITAL"			:	    0x00100000     ,
"DEBUG_PULSE" 			:       0x00200000     ,
"DEBUG_CONFIG_LOAD"		:       0x00400000     ,
"DEBUG_TRIGGERS"		:	        0x00800000 ,
"DEBUG_HS_CAPTURE"		:       0x01000000     ,
"DEBUG_ADC"   			:       0x02000000     ,
"DEBUG_BUFFMAN"			:       0x04000000     ,
"DEBUG_HW_LAYER"		:	        0x08000000 ,
"DEBUG_DRVR"  			:       0x10000000     ,
"DEBUG_APP"			    :       0x20000000     ,
"DEBUG_MQTT"  			:       0x40000000     ,
"DEBUG_BLANK10"			:       0x80000000     ,
"DEBUG_ALL"             :         0xFFFFFFFF
}

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])               #  external_stylesheets=['https://codepen.io/chriddyp/pen/bWLwgP.css']
app.config.suppress_callback_exceptions = True

# client = mqtt.Client()

mqtt_subs = ""

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Style Sheet

H1_STYLE = {'textAlign': 'left',
            'display': 'inline',
            'margin-left': '20px',
            'margin-top': '20px',
            'font-family': 'Verdana, Arial, sans-serif',
            'font-size': '12px',
            'font-weight': 'bold',
            'color': '#ffffff',
            'backgroundColor': '#C22727'}

H5_STYLE = {
    'textAlign': 'left',
     #'margin-left': '5%',
    'font-family': 'Verdana, Arial, sans-serif;',
    'font-weight': 'bold',
    'font-size': '12px',
    'color': '#3A3A3A',
    'backgroundColor': '#e8e8e8',
    'width': '45%',
    'height': '10px',
    'padding': '2%',
    'display': 'inline',
    #'position': 'relative',
    'float': 'left',
    #'left': '0%',
    #'top': '10px'
    }


H6_STYLE = {
    'textAlign': 'right',
     #'margin-left': '5%',
    'font-family': 'Verdana, Arial, sans-serif;',
    'font-size': '12px',
    'color': '#3A3A3A',
    'backgroundColor': '#e8e8e8',
    'width': '45%',
    'height': '10px',
    'padding': '2%',
    'display': 'inline',
    #'position': 'relative',
    'float': 'left',
    #'left': '0%',
    #'top': '10px'
    }

INPUT_STYLE = {
    'textAlign': 'left',
    # 'margin-left': '10px',
    'font-family': 'Verdana, Arial, sans-serif;',
    'font-size': '12px',
    'color': '#3A3A3A',
    'backgroundColor': '#FFFFFF',
    'width': '45%',
    'height': '10px',
    'padding': '2%',
    'display': 'inline',
    'border': '2px solid #616161',
    'border-color': '#616161',
    #'position': 'relative',
    'float': 'right',
    #'left': '0%',
    #'top': '10px'
    }

SELECT_STYLE = {
    'textAlign': 'left',
    # 'margin-left': '10px',
    'font-family': 'Verdana, Arial, sans-serif;',
    'font-size': '12px',
    'color': '#3A3A3A',
    'backgroundColor': '#FFFFFF',
    'width': '50%',
    'height': '250px',
    'padding': '2%',
    'display': 'inline',
    'border': '2px solid #616161',
    'border-color': '#616161',
    #'position': 'relative',
    'float': 'right',
    #'left': '0%',
    #'top': '10px'
    }

BUTTON_STYLE = {
    'textAlign': 'center',
    'color': '#3A3A3A',
    'background-color': '#e8e8e8',
    'width': '100%', 'font-size': 14,
    'display': 'block',
    'position': 'relative',
    'border': '2px solid #616161',
    'float': 'centre'}

BUTTON_SMALL_STYLE = {
    'textAlign': 'center',
    'color': '#3A3A3A',
    'background-color': '#e8e8e8',
    'width': '50%', 'font-size': 14,
    'display': 'inline',
    'position': 'relative',
    'border': '2px solid #616161',
    'float': 'centre'}

RADIO_STYLE = {
    'textAlign': 'left',
    'color': '#3A3A3A',
    'background-color': '#e8e8e8',
    'width': '45%',
    'height': '10px',
    'padding': '2%',
    'font-size': 14,
    'display': 'inline',
    'position': 'relative',
    'border': '0px solid #616161',
    'float': 'right'}

DIV_STYLE = {'textAlign': 'left',
    'font-family': 'Verdana, Arial, sans-serif;',
    'font-size': '14px',
    'font-weight': 'bold',
    'color': '#3A3A3A',
    'backgroundColor': '#e8e8e8',
    'width': '95%',
    'display': 'inline-block',
'position': 'relative',
    'float': 'left'}

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Dash Layout

# Define the GUI elements
_h1 = html.H1(children='Paradigm On-Site', style=H1_STYLE)

# Add an interval timer
_discover_timer = dcc.Interval(id='interval-discover', interval=10000, disabled=False, max_intervals =-1)
_discover_liveview = dcc.Interval(id='interval-liveview', interval=1000, disabled=False, max_intervals =-1)
_interval_debug = dcc.Interval(id='interval-debug', interval=1000, disabled=False, max_intervals =-1)

# Add the elements to control motherboard configuration
_h6_hub_ip = html.Div(children='IP Address:', style=H6_STYLE)
_input_hub_ip = dcc.Input( id="input-hub-ip", type="text", placeholder="Dave is a Legend", style=INPUT_STYLE)
_h6_hub_sn = html.Div(children='Subnet Mask:', style=H6_STYLE)
_input_hub_sn = dcc.Input( id="input-hub-sn", type="text", placeholder="Dave is a Legend", style=INPUT_STYLE)
_h6_hub_gw = html.Div(children='GW Address:', style=H6_STYLE)
_input_hub_gw = dcc.Input( id="input-hub-gw", type="text", placeholder="Dave is a Legend", style=INPUT_STYLE)
_h6_hub_serial = html.Div(children='Serial Number:', style=H6_STYLE)
_input_hub_serial = dcc.Input( id="input-hub-serial", type="text", placeholder="Dave is a Legend", style=INPUT_STYLE)
_h6_hub_location = html.Div(children='Location:', style=H6_STYLE)
_input_hub_location = dcc.Input( id="input-hub-loc", type="text", placeholder="Dave is a Legend", style=INPUT_STYLE)
_button_update_info = html.Button('Update', id='button-update-hub-info', n_clicks=0, style=BUTTON_STYLE)

HUB_INFO = [html.Div("Hub Info", style=DIV_STYLE),
            html.Div([_h6_hub_ip, _input_hub_ip], style={'display': 'block', 'padding': '20px'}),
            html.Div([_h6_hub_sn, _input_hub_sn], style={'display': 'block', 'padding': '20px'}),
            html.Div([_h6_hub_gw, _input_hub_gw], style={'display': 'block', 'padding': '20px'}),
            html.Div([_h6_hub_serial, _input_hub_serial], style={'display': 'block', 'padding': '20px'}),
            html.Div([_h6_hub_location, _input_hub_location], style={'display': 'block', 'padding': '20px'}),
            html.Div(_button_update_info, id="button-update", style={'display': 'block', 'padding': '20px'}),
            ]

# Add the elements to control motherboard configuration
_text_file_type = html.Div(children='File type:', style=H6_STYLE)
_radio_file_type = dcc.RadioItems(
    options=[
        {'label': '5 minute', 'value': '5'},
        {'label': '1 hour', 'value': '60'},
    ],
    id="radio-logging-file-type",
    value='5',
    style=RADIO_STYLE
)
_text_gps = html.Div(children='GPS:', style=H6_STYLE)
_radio_gps = dcc.RadioItems(
    options=[
        {'label': 'Log', 'value': 'gps_log'},
        {'label': 'Parse', 'value': 'gps_parse'},
    ],
    id="radio-logging-gps",
    value='gps_parse',
    style=RADIO_STYLE
)
_text_gps_preamble = html.Div(children='GPS Preamble:', style=H6_STYLE)
_input_gps_preamble = dcc.Input( id="input-gps-preamble", type="text", placeholder="$GPRMC", style=INPUT_STYLE)

_text_gps_port = html.Div(children='GPS Port:', style=H6_STYLE)
_input_gps_port = dcc.Input( id="input-gps-port", type="number", placeholder="6060", style=INPUT_STYLE)

_button_update_logging = html.Button('Update', id='button-update-logging-info', n_clicks=0, style=BUTTON_STYLE)

LOGGING_INFO = [html.Div("Logging Info", style=DIV_STYLE),
                html.Div([_text_file_type, _radio_file_type], style={'display': 'block', 'padding': '20px'}),
                html.Div([_text_gps, _radio_gps], style={'display': 'block', 'padding': '20px'}),
                html.Div([_text_gps_preamble, _input_gps_preamble], style={'display': 'block', 'padding': '20px'}),
                html.Div([_text_gps_port, _input_gps_port], style={'display': 'block', 'padding': '20px'}),
                html.Div([_button_update_logging], style={'display': 'block', 'padding': '20px'})
                ]

_text_mqtt_ip = html.Div(children='MQTT IP Address:', style=H6_STYLE)
_input_mqtt_ip = dcc.Input( id="input-mqtt-ip", type="text", placeholder="172.16.0.99", style=INPUT_STYLE)
_text_mqt_port = html.Div(children='MQTT Port:', style=H6_STYLE)
_input_mqtt_port = dcc.Input( id="input-mqtt-port", type="text", placeholder="1883", style=INPUT_STYLE)
_text_mqtt_topic = html.Div(children='MQTT Topic:', style=H6_STYLE)
_input_mqtt_topic = dcc.Input( id="input-mqtt-topic", type="text", placeholder="SN999999", style=INPUT_STYLE)
_text_mqtt_qos = html.Div(children='MQTT Quality of Service:', style=H6_STYLE)
_input_mqtt_qos = dcc.Input( id="input-mqtt-qos", type="text", placeholder="0", style=INPUT_STYLE)
_button_update_mqtt = html.Button('Update', id='button-update-mqtt-info', n_clicks=0, style=BUTTON_STYLE)

MQTT_INFO = [html.Div("MQTT Info", style=DIV_STYLE),
             html.Div([_text_mqtt_ip, _input_mqtt_ip], style={'display': 'block', 'padding': '20px'}),
             html.Div([_text_mqt_port, _input_mqtt_port], style={'display': 'block', 'padding': '20px'}),
             html.Div([_text_mqtt_topic, _input_mqtt_topic], style={'display': 'block', 'padding': '20px'}),
             html.Div([_text_mqtt_qos, _input_mqtt_qos], style={'display': 'block', 'padding': '20px'}),
             html.Div([_button_update_mqtt], style={'display': 'block', 'padding': '20px'})
            ]

_text_debug_ip = html.Div(children='Debug IP Address:', style=H6_STYLE)
_input_debug_ip = dcc.Input( id="input-debug-ip", type="text", placeholder="172.16.0.99", style=INPUT_STYLE)
_text_debug_port = html.Div(children='Debug Port Number:', style=H6_STYLE)
_input_debug_port = dcc.Input( id="input-debug-port", type="number", placeholder="4678", style=INPUT_STYLE)
_button_update_debug = html.Button('Update', id='button-update-debug-info', n_clicks=0, style=BUTTON_STYLE)
_text_debug = html.Div(children='Debug:', style=H6_STYLE)
_radio_debug = dcc.RadioItems(
    id="radio-debug",
    options=[
        {'label': 'On', 'value': 'debug_on'},
        {'label': 'Off', 'value': 'debug_off'},
    ],
    value='debug_off',
    style=RADIO_STYLE
)

_opts = [html.Option(key) for key in DEBUG_OPTS.keys()]

_select_debug_off = html.Select(_opts, size='10', multiple='multiple',name='select-debug-off', id='select-debug-off', style=SELECT_STYLE)
_select_debug_on = html.Select([], size='10', multiple='multiple',name='select-debug-on', id='select-debug-on', style=SELECT_STYLE)

_button_debug_add = html.Button('+', id='button-debug-add', n_clicks=0, style=BUTTON_SMALL_STYLE)
_button_debug_minus = html.Button('-', id='button-debug-minus', n_clicks=0, style=BUTTON_SMALL_STYLE)


DEBUG_INFO = [html.Div("Debug Info", style=DIV_STYLE),
                html.Div([_text_debug_ip, _input_debug_ip], style={'display': 'block', 'padding': '20px'}),
                html.Div([_text_debug_port, _input_debug_port], style={'display': 'block', 'padding': '20px'}),
                html.Div([_text_debug, _radio_debug], style={'display': 'block', 'padding': '20px'}),
                html.Div([_select_debug_off, _select_debug_on], style={'display': 'block', 'padding': '20px'}),
                html.Div([_button_debug_add, _button_debug_minus], style={'display': 'block', 'padding': '20px'}),
                html.Div([_button_update_debug], style={'display': 'block', 'padding': '20px'})
              ]

_text_liveview = html.Div(children='Liveview:', style=DIV_STYLE)
_graph_liveview = dcc.Graph(id='live-update-graph', style={'display': 'block', "height": "50pc"})
_text_debug = html.Div(children='Debug Log:', style=DIV_STYLE)
_textarea_debug = html.Div("", id="textarea-debug", style={
    'display': 'block', 'overflow': 'auto',  'white-space': 'pre-wrap',
    'width': '100%', 'height': '25pc',
    'padding': '20px', 'border': '2px solid #616161'})

# Pop up windows
_modal_hub_info = dbc.Modal(
            [
                dbc.ModalHeader("Header", id='modal-hub-info-header', style={'color': '#C22727'}),
                dbc.ModalBody("This is the content of the modal", id='modal-hub-info-body'),
                dbc.ModalFooter(
                    dbc.Button("Close", id="modal-hub-info-close", className="ml-auto")
                ),
            ],
            id="modal-hub-info",
            #size="sm",
            is_open=False,
            centered=True,
            style={'z-index': '1'})

_modal_hub_debug = dbc.Modal(
            [
                dbc.ModalHeader("Header", id='modal-hub-debug-header', style={'color': '#C22727'}),
                dbc.ModalBody("This is the content of the modal", id='modal-hub-debug-body'),
                dbc.ModalFooter(
                    dbc.Button("Close", id="modal-hub-debug-close", className="ml-auto")
                ),
            ],
            id="modal-hub-debug",
            #size="sm",
            is_open=False,
            centered=True,
            style={'z-index': '1'})



# Group the elements into DIVS
LAYOUT = html.Div([
    # Add the page content
    html.Div(_h1, style={'width': '100%', 'float': 'center', 'display': 'inline-block', 'height': '35px', 'backgroundColor': '#C22727'}),

    dcc.Location(id='url', refresh=False),

    # Add the popups
    _modal_hub_info,
    _modal_hub_debug,

    # Add the timers
    _discover_timer,
    _discover_liveview,
    _interval_debug,

    html.Div([_text_liveview, _graph_liveview, _text_debug, _textarea_debug], style={'width': '66%', "height": "100pc", 'float': 'right', 'display': 'block', 'margin-top': '1%', 'margin-right': '1%'}),

    html.Div(HUB_INFO, style={'width': '30%', 'float': 'left', 'display': 'block', 'border-style': 'solid', 'border-color': '#616161', 'margin-top': '1%', 'margin-left': '1%'}),
    html.Div(LOGGING_INFO, style={'width': '30%', 'float': 'left', 'display': 'block', 'border-style': 'solid', 'border-color': '#616161', 'margin-top': '1%', 'margin-left': '1%'}),
    html.Div(MQTT_INFO, style={'width': '30%', 'float': 'left', 'display': 'block', 'border-style': 'solid', 'border-color': '#616161', 'margin-top': '1%', 'margin-left': '1%'}),
    html.Div(DEBUG_INFO, style={'width': '30%', 'float': 'left', 'display': 'block', 'border-style': 'solid', 'border-color': '#616161', 'margin-top': '1%', 'margin-left': '1%'}),
    #html.Div(HUB_DATA, style={'width': '70%', 'float': 'left', 'display': 'inline-block'})
    #html.Div(HUB_LOG, style={'width': '100%', 'float': 'left', 'display': 'inline-block'})
    ], style={'width': '100%', 'float': 'center', 'height': '100pc', 'display': 'inline-block', 'backgroundColor': '#e8e8e8'})

# Group the elements into DIVS
app.layout = LAYOUT

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Classes

hub = hub_import.Hub()


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Functions



# Add the timer callback function
timers.callback_interval_debug(app)
timers.callback_interval_hub_scan(app)
timers.callback_interval_liveview(app)

# Add the button callback functions
buttons.callback_reprogram_debug_info(app)
buttons.callback_reprogram_hub_info(app)
buttons.callback_update_debug_options(app)



# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Main


if __name__ == "__main__":
    '''
    The main acts as the main hub for inputs requred to run the golden tool.
    It will get the information required from the user and then call the
    appropriate function. The input function should only ever be called in
    here.
    '''

    """
    client._client_id = "python_client_id"
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("127.16.0.98", 1883, 30)

    print("looping")
    client.loop_start() # Blocks for 100ms
    """

    print("DASH VERSION: {}".format(dcc.__version__))
    app.run_server(debug=True)