#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep  6 15:31:14 2018

@author: davesmith
"""

import socket
#from xml_builder import XMLPkt
import xml.etree.ElementTree as ET
import time
from datetime import timedelta

class udp_socket:
    def __init__(self, local_ip_addr):
        self.local_ip_addr = local_ip_addr
        self.udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.udp.settimeout(.1)
        self.udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.udp.bind((self.local_ip_addr, 46787))

    def update(self, a_ip_addr):
        self.local_ip_addr = a_ip_addr
        self.udp.bind((self.local_ip_addr, 46787))

    def receive(self):
        _all_data = []

        while True:

            try:
                _data, addr = self.udp.recvfrom(1000)
                # print(_data, addr)

            except socket.timeout:
                #print("Socket timeout")
                break

            if _data is None:
                break

            if addr[0] != self.local_ip_addr:
                _all_data.append(_data)

        return _all_data

    def send(self, a_command):
        '''
        Send a request discover to get the current ip address held in RAM
        '''
        _return = []

        xml_byte_array = a_command.encode('utf-8')

        # print("Tx: {}".format(xml_byte_array))
        self.udp.sendto(xml_byte_array, ('255.255.255.255', 46787))

        """
        try:

            data, addr = self.udp.recvfrom(1000)

            if addr[0] == self.local_ip_addr:
                data, addr = self.udp.recvfrom(1000)
                return _data
                _return.append((data, addr))

        except Exception as e:
            print("udp send", e)

        self.udp.close()
        return _return
        """

def udp_discover(local_ip_addr):
    '''
    Send a request discover to get the current ip address held in RAM
    '''
    xmlStatus = XMLPkt()
    xmlStatus.tagRoot = "request"
    xmlStatus.tagName = "discover"

    etxml = xmlStatus.generateXMLElement()
    xmlString = ET.tostring(etxml)
    xmlString = xmlString.decode('utf-8')
    xmlString = xmlString + '\x0D\x0A'

    xml_byte_array = xmlString.encode('utf-8')

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.settimeout(1)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    udp.bind((local_ip_addr, 46787))

    udp.sendto(xml_byte_array, ('255.255.255.255', 46787))

    try:
        while True:
            data, addr = udp.recvfrom(1000)

            if addr[0] != local_ip_addr:
                break

    except Exception as e:
        print(e)
        return None, None

    udp.close()
    return (data, addr)


def udp_listen(local_ip_addr, a_port_number):
    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.settimeout(.1)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    udp.bind((local_ip_addr, a_port_number))

    _all_data = ""
    try:
        while True:
            data, addr = udp.recvfrom(1000)

            if data is None:
                break

            print("udp_listen", data)
            _all_data += data.decode("utf8")


    except Exception as e:
        #print(e)
        data = None
        addr = None

    udp.close()

    return _all_data, addr


class UDP_Conn:

    def __init__(self, port=None):
        self.g_udp = None
        self.port = 46787
        if port is not None:
            print ("port reset to{}".format(port))
            self.port = port

    def udp_open(self, local_ip_addr):
        print("opening UDP port on {}".format(self.port))
        self.g_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.g_udp.settimeout(0.2)
        self.g_udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.g_udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.g_udp.bind((local_ip_addr, self.port))

    def udp_close(self):
        self.g_udp.close()

    def udp_get(self):
        try:
            data, addr = self.g_udp.recvfrom(1000)
        except Exception as e:
            #print(e)
            data = None
            addr = None

        return data, addr

def udp_reboot(local_ip_addr):
    '''
    Send a request discover to get the current ip address held in RAM
    '''
    xmlStatus = XMLPkt()
    xmlStatus.tagRoot = "request"
    xmlStatus.tagName = "reboot"

    etxml = xmlStatus.generateXMLElement()
    xmlString = ET.tostring(etxml)
    xmlString = xmlString.decode('utf-8')
    xmlString = xmlString + '\x0D\x0A'

    xml_byte_array = xmlString.encode('utf-8')

    udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp.settimeout(5)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    udp.bind((local_ip_addr, 46787))

    udp.sendto(xml_byte_array, ('255.255.255.255', 46787))

    udp.close()
