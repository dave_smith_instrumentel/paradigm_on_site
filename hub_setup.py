# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 12:14:50 2019

@author: Dave.Smith
"""

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

import os
import collections
import socket
import sys
# import main
import time
import struct
import pytz
import base64

from datetime import datetime
import tcp_handler as tcp
import udp_handler as udp

import Internal_NetworkTools.processing.conf_gen as conf_gen

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants

# We need to following folders to download, process and view data
WORKING_DIR = os.path.dirname(os.path.realpath(__file__))
RAW_DIR = os.path.join(WORKING_DIR, 'data', 'raw')
PROCESSED_DIR = os.path.join(WORKING_DIR, 'data', 'processed')
ANALYSIS_DIR = os.path.join(WORKING_DIR, 'data', 'analysis')


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Local Functions

def build_message(a_string):
    """

    :return:
    """
    _preamble = "$INST".encode('utf-8')
    _payload = a_string.encode('utf-8')
    _crc_16 = gen_crc16(_payload)

    _return = _preamble + _payload + bytearray([(_crc_16 >> 8) & 0xFF, _crc_16 & 0xFF])

    return _return

    #_reply = tcp.send_bytearray(a_hub, _preamble + _payload + bytearray([(_crc_16 >> 8) & 0xFF, _crc_16 & 0xFF]))

def print_bin_as_hex(data):
    return "".join(["{:02X}".format(x) for x in data])

def gen_crc16(data):
    size = len(data)
    data_index = 0
    out = 0;  # 16b
    bits_read = 0
    bit_flag = 0

    while (size > 0):
        bit_flag = out >> 15;

        # Get next bit: */
        out <<= 1;
        out |= ((data[data_index] >> bits_read) & 1)  # item a) work from the least significant bits
        out = out & 0xFFFF

        # Increment bit counter: */
        bits_read += 1;
        if (bits_read > 7):
            bits_read = 0;
            data_index += 1;
            size -= 1;

        # Cycle check: */
        if (bit_flag):
            out ^= 0x8005;
            out &= 0xFFFF

    # item b) "push out" the last 16 bits
    for i in range(16):
        bit_flag = out >> 15;
        out <<= 1;
        out = out & 0xFFFF

        if (bit_flag):
            out ^= 0x8005;
            out &= 0xFFFF

    # item c) reverse the bits
    crc = 0;
    i = 0x8000;
    j = 0x0001;

    # for (; i != 0; i >>=1, j <<= 1)
    while (i != 0):

        if (i & out):
            crc |= j;

        i >>= 1
        j <<= 1

    return crc

def list_files_in_dir(directory, string):
    count = 1

    for d, subdir, files in os.walk(directory):
        for f in files:
            print(count, string, f)
            count += 1

    return files

# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Global Functions

def discover_hub(a_udp_socket, a_hub):
    # data, addr = udp.udp_discover(local_ip_addr)
    print("Discover hub")
    _data = a_udp_socket.send("discover")
    _data = a_udp_socket.receive()

    if _data == None:
        return False

    """
    if len(_hubs) == 0:
        data = None
        addr = None

    elif len(_hubs) == 1:
        data = _hubs[0][0]
        addr = _hubs[0][1]

    else:
        data = _hubs[0][0]
        addr = _hubs[0][1]
        
        print(_hubs)

        for x in range(len(_hubs)):
            _hubs_parts = _hubs[x][0].decode("utf-8").split(",")

            if len(_hubs_parts) < 5:
                # print(_hub_parts)
                pass
            else:
            
                print("{}. {}, {}".format(x, _hubs_parts[4], _hubs_parts[5]))

        _cmd = input("Please select which Hub you want to communicate with? ")

        data = _hubs[int(_cmd)][0]
        addr = _hubs[int(_cmd)][1]
        """
    """
    if data == None or addr == None:
        print("No response.")
        return False
    """

    for _packet in _data:
        if a_hub.populate_hub_details(_packet):
            break

    # Check the IP address of the hub against our local IP to see if we can
    # open a TCP connection.
    hub_sn_addr = [0, 0, 0, 0]
    local_sn_addr = [0, 0, 0, 0]

    local_ip = a_udp_socket.local_ip_addr.split(".")
    local_ip = list(map(int, local_ip))

    for x in range(4):
        hub_sn_addr[x] = a_hub.sn[x] & a_hub.ip[x]
        local_sn_addr[x] = a_hub.sn[x] & local_ip[x]

    """
    if hub_sn_addr == local_sn_addr:
        print("We are on the network.")
    else:
        print("We aren't on the network. Change your IP address from",
              local_ip_addr, "to the same network as", hub.ip)
              """

    #print(hub)

    return hub_sn_addr == local_sn_addr

def db_firmware_menu():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("1. Erase flash bank to be used for DB's firmware.")
    print("2. Read flash - **FOR TESTING ONLY, USE LM FLASH PROGRAMMER INSTEAD**.")
    print("3. Check the version of DB's firmware.")
    print("4. Send bin file to be programmed into MB flash.")
    print("5. Put DB into ROM bootloader mode.")
    print("6. Update DB with the new firmware.")

def db_firmware(hub):

    db_firmware_menu()

    _cmd = input("Enter the number of what you would like to do: ")
    _cmd = int(_cmd)

    #print(_cmd, type(_cmd))
    
    if not os.path.exists(os.path.join(WORKING_DIR, 'FW0070', 'db_bin')):
        os.makedirs(os.path.join(WORKING_DIR, 'FW0070', 'db_bin'))
    
    _reply = None
    
    # Erase a specific flash bank on MB (start addr: 0x80000) before sending the new firmware that will be used to update DB
    if _cmd == 1:
        global _contents_len
        
        bin_dir = os.path.join(WORKING_DIR, 'FW0070', 'db_bin')
        
        #if not os.path.exists(bin_dir)
        #    os.makedirs(os.path.join(WORKING_DIR, 'FW0070', 'db_bin'))
        
        files = list_files_in_dir(bin_dir, "Available Binary Files:")
        
        bin_file = input("Which BIN file would you like to use? ")
        filepath = os.path.join(bin_dir, files[int(bin_file)-1])
        print(filepath)
        
        with open(filepath, 'rb') as f:
            _data = f.read()  # read the first 4 data
        
        print("Size of the BIN file is: {}".format(len(_data)))
        
        _msg_str = "dbfirmware,erase_flash={}:{}".format(len(_data), 0)
        
        _msg = build_message(_msg_str)
        
        _reply = tcp.send_bytearray(hub, _msg)
        
        _contents_len = 0;
    
    # Read the flash to compare the B64 string of the BIN file with the B64 string that is stored in MB's flash
    if _cmd == 2:
        print(f"Len of contents written in flash: {_contents_len}")
        
        CHUNK_SIZE = 1023 # = 341 * 3
        
        for x in range(0, _contents_len, CHUNK_SIZE):
            _msg_str = "dbfirmware,read_flash={}:{}".format(x, CHUNK_SIZE)
            _msg = build_message(_msg_str)
            _reply = tcp.send_bytearray(hub, _msg)
                    
    # Check DB's firmware version
    if _cmd == 3:
        _msg = build_message("dbfirmware,version")
        print(_msg)
        
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)
        
        hex_data = print_bin_as_hex(_reply)
        print(hex_data)

    # Program the new DB's firmware into MB's flash
    if _cmd == 4:
    
        bin_dir = os.path.join(WORKING_DIR, 'FW0070', 'db_bin')
        
        #if not os.path.exists(bin_dir)
        #    os.makedirs(os.path.join(WORKING_DIR, 'FW0070', 'db_bin'))
        
        files = list_files_in_dir(bin_dir, "Available Binary Files:")
        
        bin_file = input("Which BIN file would you like to use? ")
        filepath = os.path.join(bin_dir, files[int(bin_file)-1])
        
        print(filepath)
        
        with open(filepath, 'rb') as f:
            _contents = f.read()  # read the first 4 data
        
        '''
        # Generate a CRC32 (the CRC32 can also be verified with the LM Flash Programmer)
        _checksum_of_data = gen_crc16(_contents)
        print(f"_checksum_of_data: {_checksum_of_data}")
        '''
        #_contents_len = len(_contents)
        print("Size of the BIN file is: {}".format(len(_contents)))
        
        _contents = struct.pack("<I", len(_contents)) + _contents
        
        CHUNK_SIZE = 900 # = 341 * 3
        
        for x in range(0, len(_contents), CHUNK_SIZE):
            _binchunk = _contents[x:x+CHUNK_SIZE]
            print("Size of BIN chunk is: {}".format(len(_binchunk)))
            
            if len(_binchunk) %4 != 0:
                # need to round up nicely to 4 bytes
                _binchunk += bytearray([0]*(4-(len(_binchunk) %4)))
                print("new length:", len(_binchunk))
                
            _b64chunk = base64.b64encode(_binchunk).decode('ascii')
            print(len(_b64chunk))
            
            _msg_str = "dbfirmware,bin={}:{}:{}".format(x, len(_b64chunk), _b64chunk)
            _msg = build_message(_msg_str)
            
            _reply = tcp.send_bytearray(hub, _msg)
            
            #break
    
    # Put DB into bootloader mode before updating
    if _cmd == 5:
        _msg = build_message("dbfirmware,blmode")
        
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)
        
    # Start the update process of the DB
    if _cmd == 6:
        _msg = build_message("dbfirmware,update_db")
        
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

def change_menu():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("1. Change the IP address.")
    print("2. Change the SN mask.")
    print("3. Change the GW address.")
    print("4. Change the serial number.")
    print("5. Change the location.")
    print("6. Change the db config.")
    print("7. Set the time.")
    print("8. Change the file type.")
    print("9. Set the GPS zones.")
    print("10. Set the UDP IP address.")
    print("11. Set the UDP port.")
    print("12. Set the GPS port number.")
    print("13. Parse the GPS data.")
    print("14. Set the GPS preamble to parse.")
    print("15. Send the tag strip calibration.")
    print("16. Set the MQTT IP address.")
    print("17. Set the MQTT port number.")
    print("18. Set the MQTT publish topic.")
    print("19. Commit the changes to flash.")

def change(hub):
    """

    :param hub:
    :return:
    """

    change_menu()
    _cmd = input("Enter the number of what you would like to do: ")
    _cmd = int(_cmd)

    print(_cmd, type(_cmd))

    # Set a new IP address
    if _cmd == 1:
        _addr = input("Enter the new address: ")
        ip_addr_raw = _addr.split(".")
        ip_addr_raw = list(map(int, ip_addr_raw))

        for byte in ip_addr_raw:
            if byte < 0 or byte > 255:
                print("IP address not valid", str(byte))
                return False

        _msg = build_message("change,ip={}".format(_addr))
        print("Tx: {}".format(_msg))

    # Set a new SN address
    if _cmd == 2:
        _addr = input("Enter the new address: ")
        ip_addr_raw = _addr.split(".")
        ip_addr_raw = list(map(int, ip_addr_raw))

        for byte in ip_addr_raw:
            if byte < 0 or byte > 255:
                print("SN address not valid", str(byte))
                return False

        _msg = build_message("change,sn={}".format(_addr))
        print("Tx: {}".format(_msg))

    # Set a new GW address
    if _cmd == 3:
        _addr = input("Enter the new address: ")
        ip_addr_raw = _addr.split(".")
        ip_addr_raw = list(map(int, ip_addr_raw))

        for byte in ip_addr_raw:
            if byte < 0 or byte > 255:
                print("GW address not valid", str(byte))
                return False

        _msg = build_message("change,gw={}".format(_addr))
        print("Tx: {}".format(_msg))

    # Set a new serial number
    if _cmd == 4:
        _serial = input("Enter a serial number: ")
        _msg = build_message("change,serial={}".format(_serial))
        print("Tx: {}".format(_msg))

    # Set a new location
    if _cmd == 5:
        _location = input("Enter a location: ")
        _msg = build_message("change,location={}".format(_location))
        print("Tx: {}".format(_msg))

    # Send the Hitachi location
    elif _cmd == 6:
        config_dir = os.path.join(WORKING_DIR, 'FW0070', 'configs', 'customer') #TODO - signals back to customer DS 1/7/2020
        files = list_files_in_dir(config_dir, "Config")

        json = input("Which JSON would you like to use? ")

        hub.db_config = conf_gen.ConfigBuilder()
        hub.db_config.load_json_file(os.path.join(config_dir, files[int(json)-1]))
        print("Loaded Signals: {}, Sequences: {} ".format(len(hub.db_config.signals), len(hub.db_config.sequences)))

        hub.db_config_b64 = hub.db_config.return_b64()
        hub.db_config.print_list()
        
        print(hub.db_config_b64)
        print("Len of B64 string: {}".format(len(hub.db_config_b64)))
        print(type(hub.db_config_b64))
        
        # Check if the config has a size greater than 1300. 
        # This is used to make sure that the config string is not greater than the TCP buffer.
        # Has been raised as an issue in JIRA (FR-273)
        if len(hub.db_config_b64) > 1300:
            for x in range(0, len(hub.db_config_b64), 1024):
                _conf_chunk = hub.db_config_b64[x:x+1024]
                
                print(_conf_chunk)
                _msg = build_message("change,db_conf_parts={}:{}".format(x, _conf_chunk))
                _reply = tcp.send_bytearray(hub, _msg)
                
            _msg = None
        else:
            _msg = build_message("change,db_conf={}".format(hub.db_config_b64))

    # Change the time
    if _cmd == 7:
        _utc_dt = datetime.utcnow()
        _utc_dt = _utc_dt.replace(tzinfo=pytz.utc)
        _utc_ts = int(datetime.timestamp(_utc_dt))
        _msg = build_message("change,time={}".format(_utc_ts))
        print("Tx: {}".format(_msg))

    # Change the time back
    elif _cmd == 8:

        reply = input("One hour files? (true, false) ")

        if reply == 'true':
            _msg = build_message("change,file_type=1")

        elif reply == 'false':
            _msg = build_message("change,file_type=0")

        else:
            print("Incorrect command", reply)
            return

    elif _cmd == 9:
        _msg = build_message(
            "change,gps_zone=0:51.309729:0.897556:51.310097:0.897738:51.310827:0.893233:51.311151:0.893178")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=1:51.340212:1.401805:51.341950:1.407284:51.342321:1.399919:51.343713:1.404839")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=2:51.139882:0.882895:51.141282:0.884506:51.141945:0.878795:51.142963:0.879910")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=3:51.437441:0.303181:51.443897:0.334454:51.453848:0.287397:51.464111:0.313723")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=4:51.136919:0.874241:51.147538:0.889182:51.152057:0.848703:51.157095:0.865647")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=5:51.533363:-0.130120:51.534217:-0.123855:51.539609:-0.132567:51.540570:-0.1266129")
        _reply = tcp.send_bytearray(hub, _msg)
        _msg = build_message(
            "change,gps_zone=6:51.542906:-0.012250:51.542025:-0.004911:51.546375:-0.011692:51.546829:-0.005469")
        #_reply = tcp.send_bytearray(hub, _msg)

    elif _cmd == 10:
        _addr = input("Enter the new address: ")
        ip_addr_raw = _addr.split(".")
        ip_addr_raw = list(map(int, ip_addr_raw))

        for byte in ip_addr_raw:
            if byte < 0 or byte > 255:
                print("GW address not valid", str(byte))
                return False

        _msg = build_message("change,udp_ip={}".format(_addr))
        print("Tx: {}".format(_msg))

        #_msg = build_message("change,udp_ip=172.16.0.162")

    elif _cmd == 11:
        _port = input("Enter the UDP port: ")

        try:
            _port = int(_port)
            _msg = build_message("change,udp_port={}".format(_port))

        except Exception as e:
            print("Incorrect port number.")
            return None

    elif _cmd == 12:
        _port = input("Set the GPS port? ")
        _msg = build_message("change,gps_port={}".format(_port))

    elif _cmd == 13:
        reply = input("Set the GPS parser? Otherwise just log straight to the SD card. (true, false) ")

        if reply == 'true':
            _msg = build_message("change,gps_parse=1")

        elif reply == 'false':
            _msg = build_message("change,gps_parse=0")

        else:
            print("Incorrect command", reply)
            return

    elif _cmd == 14:
        _preamble = input("Set the GPS preamble to use when parsing? ")

        if len(_preamble) <= 8:
            _msg = build_message("change,gps_preamble={}".format(_preamble))

        else:
            print("Too many chars, should be equal to 6.")
            return

    elif _cmd == 15:
        calib_dir = os.path.join(WORKING_DIR, 'FW0075', 'calibs', 'customer')
        files = list_files_in_dir(calib_dir, "Calibration")

        json = input("Which JSON would you like to use? ")

        hub.builder = conf_gen.CalibrationBuilder()
        hub.builder.load_json_file(os.path.join(calib_dir, files[int(json) - 1]))
        print("Calibration: {} ".format(len(hub.builder.calibration_data)))

        hub.builder_64 = hub.builder.return_b64()
        hub.builder.print_list()
        print(hub.builder_64)

        _msg = build_message("change,db_calib={}".format(hub.builder_64))

    elif _cmd == 16:
        _addr = input("Enter the new address: ")
        ip_addr_raw = _addr.split(".")
        ip_addr_raw = list(map(int, ip_addr_raw))

        for byte in ip_addr_raw:
            if byte < 0 or byte > 255:
                print("IP address not valid", str(byte))
                return False

        _msg = build_message("change,mqtt_addr={}".format(_addr))
        print("Tx: {}".format(_msg))

        #_msg = build_message("change,udp_ip=172.16.0.162")

    elif _cmd == 17:
        _port = input("Enter the MQTT port: ")

        try:
            _port = int(_port)
            _msg = build_message("change,mqtt_port={}".format(_port))

        except Exception as e:
            print("Incorrect port number.")
            return None

    elif _cmd == 18:
        _topic = input("Enter the MQTT topic: ")

        try:
            _msg = build_message("change,mqtt_topic={}".format(_topic))

        except Exception as e:
            print("Incorrect topic.")
            return None

    elif _cmd == 19:
        _msg = build_message("change,commit=1")
    
    # Send message if only there is a message to send
    if _msg is not None:
        _reply = tcp.send_bytearray(hub, _msg)

    return _reply


def action_menu():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("1. Discover the hub.")
    print("2. List the contents of the SD card.")
    print("3. Remove a file.")
    print("4. How much disk is free?")
    print("5. Download a file.")
    print("6. Format the SD card.")
    print("7. Reboot the hub.")
    print("8. Forboot.")
    print("9. soft_trigger")
    print("10. hard_trigger")
    print("11. db_conf")
    print("12. DDU learn tags.")
    print("13. DDU Test Read Range/Quality")
    print("15. db_cali")
    print("15. Put the hub into ROM Bootloader (Ethernet)")


def action(hub):
    """

    :param information_param:
    :param hub:
    :param spare:
    :return:
    """
    action_menu()
    _cmd = input("Enter the number of what you would like to do: ")
    _cmd = int(_cmd)

    print(_cmd, type(_cmd))

    _reply = None

    if _cmd == 1:
        while True:
            _msg = build_message("action,discover")
            _reply = tcp.send_bytearray(hub, _msg)
            print(_reply)
            time.sleep(1)

    elif _cmd == 2:
        File = collections.namedtuple('File', 'folder file size')

        # Find a file on the hub to download
        _msg = build_message("action,cd=/,ls")
        _reply = tcp.stream_bytearray(hub, _msg)

        if _reply is False:
            print("Hub failed to respond.")
            return

        print("Tx: {}".format(_msg))
        print("Rx: {}".format(_reply))

        _dirs = _reply.decode('utf-8').split(",")
        hub.dirs = _dirs[1:-1]
        hub.files = []

        for _dir in _dirs[1:-1]:
            _msg = build_message("action,cd=/{},ls".format(_dir))
            _reply = tcp.stream_bytearray(hub, _msg)
            print("Tx: {}".format(_msg))
            print("Rx: {}".format(_reply))

            _files =  _reply.decode().split(",")

            for _file in _files[1:-1]:
                print(_file)
                hub.files.append(File(folder=_dir, file=_file.split(":")[0], size=_file.split(":")[1]))

    elif _cmd == 3:
        _counter = 0

        for _dir in hub.dirs:
            print("{}. {}".format(_counter, _dir))
            _counter += 1

        _cmd = input("Which folder would you like to delete? ")
        _cmd = int(_cmd)

        _msg = build_message("action,rm=/{}".format(hub.dirs[_cmd]))
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 4:
        _msg = build_message("action,df")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 5:
        _counter = 0

        for _file in hub.files:
            print("{}. {}".format(_counter, _file))
            _counter += 1

        _cmd = input("Which file would you like to download? ")
        _cmd = int(_cmd)

        print("Downloading cct={}:{}".format(hub.files[_cmd].folder, hub.files[_cmd].file))

        # Build the location to save the file
        #_dir_path = ""
        #for folder in os.path.join(main.RAW_DIR, hub.location, hub.files[_cmd].folder).split(os.sep):
        _dir_path = os.path.join(main.RAW_DIR, hub.location, hub.files[_cmd].folder)

        if not os.path.exists(_dir_path):
            print(_dir_path)
            os.makedirs(_dir_path)

        _prev_data = {"data":0, "time":time.time()}
        _kwargs_callback = {"filename":hub.files[_cmd].file, "data":_prev_data}

        def _down_callback(finished=None, data_in=-1, filename=hub.files[_cmd].file, data=None):

            #Calc data rate
            _speed = None 
            _eta = None

            if data is not None:
                _data_delta = data_in - data["data"]
                #print ("_data_delta", _data_delta)
                _now = time.time()
                _delta_time = _now - data["time"]
                #print ("_delta_time", _delta_time)
                
                data["data"] = data_in
                data["time"] = _now 

                if _delta_time > 0:
                    _speed = _data_delta/_delta_time
                    #print ("_speed", _speed)
                    #print ("type", hub.files[_cmd].size, type(hub.files[_cmd].size))

            if _speed is not None:
                # calc seconds left
                _size_tot = int(hub.files[_cmd].size)
                _data_remain = _size_tot - data_in
                _eta = _data_remain / _speed
                _percentage = (data_in / _size_tot) * 100
                
            print("Complete: {}%, data: {} of {} fin: {} speed: {} eta : {}".format(_percentage, data_in, filename, finished, _speed, _eta))

        _msg = build_message("action,cct={}:{}:0:0".format(hub.files[_cmd].folder, hub.files[_cmd].file))
        _reply = tcp.stream_bytearray(hub, _msg, call_back=_down_callback, call_back_amount=102400, call_back_kwargs=_kwargs_callback)
        _reply = _reply[:-11]
        print("Tx: {}".format(_msg))
        #print("Rx: {}".format(_reply))

        if len(_reply) != int(hub.files[_cmd].size):
            print("File size mismatch. Expected: {} Actual: {}".format(hub.files[_cmd].size, len(_reply)))

        with open(os.path.join(main.RAW_DIR, hub.location, hub.files[_cmd].folder, hub.files[_cmd].file), "wb") as f:
            f.write(_reply)

    elif _cmd == 6:
        _msg = build_message("action,format")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 7:
        _msg = build_message("action,reboot")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 8:
        _msg = build_message("action,forboot")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 9:
        _msg = build_message("action,soft_trigger=AAAAAICEHgCAGgYA")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 10:
        _msg = build_message("action,hard_trigger")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    elif _cmd == 11:
        _msg = build_message("action,db_conf")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)
    
        # Learn new tags
    elif _cmd == 12:
        learn_dur = int(input("For how long you want to run this process:"))
        max_tags_learn = int(input("How many tags you want to learn: "))

        # Turn on ddu_liveview and send the command to the hub
        _msg = build_message("action,ddu_liveview=on")
        _reply = tcp.send_bytearray(hub, _msg)

        ddu_tool.udp_ddu_learn_tags(learn_dur, max_tags_learn)

        # Turn off ddu_liveview when timing out and send the command to the hub
        _msg = build_message("action,ddu_liveview=off")
        _reply = tcp.send_bytearray(hub, _msg)

    # Learn tags profile
    elif _cmd == 13:
        # Turn on ddu_liveview and send the command to the hub
        _msg = build_message("action,ddu_liveview=on")
        _reply = tcp.send_bytearray(hub, _msg)

        ddu_tool.udp_ddu_read_range()

        # Turn off ddu_liveview when timing out and send the command to the hub
        _msg = build_message("action,ddu_liveview=off")
        _reply = tcp.send_bytearray(hub, _msg)
    	
    elif _cmd == 14:
        _msg = build_message("action,db_calib")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)
        
    elif _cmd == 15:
        _msg = build_message("action,rom_ethernet")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)
        


def list_hub(hub):
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    File = collections.namedtuple('File', 'folder file size')

    # Find a file on the hub to download
    _msg = build_message("action,cd=/,ls")
    print("Tx: {}".format(_msg))

    _reply = tcp.stream_bytearray(hub, _msg)

    if _reply is False:
        print("Hub failed to respond.")
        return

    print("Rx: {}".format(_reply))
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

    _dirs = _reply.decode('utf-8').split(",")
    hub.dirs = _dirs[1:-1]
    hub.files = []

    for _dir in _dirs[1:-1]:
        _msg = build_message("action,cd=/{},ls".format(_dir))
        _reply = tcp.stream_bytearray(hub, _msg)

        if _reply is False:
            print("Hub failed to respond.")
            continue

        print("Tx: {}".format(_msg))
        print("Rx: {}".format(_reply))
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

        _files =  _reply.decode().split(",")

        for _file in _files[1:-1]:
            hub.files.append(File(folder=_dir, file=_file.split(":")[0], size=_file.split(":")[1]))

def download_file(hub, file):
    """
    """
    _prev_data = {"data":0, "time":time.time()}
    _kwargs_callback = {"filename":file.file, "data":_prev_data}

    def _down_callback(finished=None, data_in=-1, filename=file.file, data=None):

        #Calc data rate
        _speed = None 
        _eta = None

        if data is not None:
            _data_delta = data_in - data["data"]
            #print ("_data_delta", _data_delta)
            _now = time.time()
            _delta_time = _now - data["time"]
            #print ("_delta_time", _delta_time)
            
            data["data"] = data_in
            data["time"] = _now 

            if _delta_time > 0:
                _speed = _data_delta/_delta_time
                #print ("_speed", _speed)
                #print ("type", hub.files[_cmd].size, type(hub.files[_cmd].size))

        if _speed is not None:
            # calc seconds left
            _size_tot = int(file.size)
            _data_remain = _size_tot - data_in
            _eta = _data_remain / _speed
            _percentage = (data_in / _size_tot) * 100
            
        print("Complete: {}%, data: {} of {} fin: {} speed: {} eta : {}".format(_percentage, data_in, filename, finished, _speed, _eta))
    
    
    _msg = build_message("action,cct={}:{}:0:0".format(file.folder, file.file))
    _reply = tcp.stream_bytearray(hub, _msg, call_back=_down_callback, call_back_amount=102400, call_back_kwargs=_kwargs_callback)

    return _reply

def debug_menu():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("1. Turn on live view.")
    print("2. Turn on additional debugging.")
    print("3. Turn off safe mode.")


def debug(hub):
    """

    :param information_param:
    :param hub:
    :param spare:
    :return:
    """
    debug_menu()
    _cmd = input("Enter the number of what you would like to do: ")
    _cmd = int(_cmd)

    print(_cmd, type(_cmd))

    # Liveview
    if _cmd == 1:

        _msg =  build_message("debug,db_conf")
        _reply = tcp.send_bytearray(hub, _msg)
        print("DB conf: {}".format(_reply))

        _msg = build_message("debug,liveview=on")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

        # Convert the daughterboard config into a conf object
        #config = conf_gen.ConfigBuilder()
        #config.decode_b64(_db_conf)


    # debug sources
    if _cmd == 2:
        _msg = build_message("debug,on=131")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)

    # debug sources
    if _cmd == 3:
        _msg = build_message("debug,safe_mode=0")
        _reply = tcp.send_bytearray(hub, _msg)
        print(_reply)


##########################################################
####### P|UT THIS INTO THE DEBUG SECTION


# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 12:03:56 2019

@author: Dave.Smith
"""

import time
import threading
import os


import udp_handler as udp


def change_logging_ip_settings(hub, ip, port):
    try:
        pass
        #config.send("debug", "ip", "172.16.1.162", hub)
        #config.send("debug", "port", "46777", hub)

    except Exception as e:
        print(e)



def change_logging_source(on, hub):
    print("1. UDP")
    print("2. TCP")
    print("3. RTC")
    print("4. MICRO SD")
    print("5. FLASH")
    print("6. SYS CTRL")
    print("7. HUB CLIENT")
    print("8. UART")
    print("9. WATCHDOG")
    print("10. RING BUFFER")

    if on:
        debug_level = input("Turn on? ")
    else:
        debug_level = input("Turn off? ")

    try:
        debug_level = 1 << int(debug_level)

        if on:
            pass
            #config.send("debug", "on", str(debug_level), hub)
        else:
            pass
            #config.send("debug", "off", str(debug_level), hub)

    except Exception as e:
        print(e)


def enabling_udp_logging(ip_addr, hub, log_dir):
    print(hub.ip, ip_addr)

    ip_str = (str(hub.ip[0]) + '.' + str(hub.ip[1]) + '.' +
              str(hub.ip[2]) + '.' + str(hub.ip[3]))
    duration = input("How long to log debug for? ")

    try:
        duration = int(duration)

        #config.send("debug", "timer", str(duration), hub)

        debug_thread = threading.Thread(target=udp_listen, args=[ip_addr, ip_str, duration, log_dir, hub])

        debug_thread.start()

        return debug_thread

    except Exception as e:
        # print(e)
        return None


def udp_listen(ip_addr, ip_str, duration, log_dir, hub):

    filepath = os.path.join(log_dir, hub.location + '.txt')

    if not os.path.isfile(filepath):
        file = open(filepath, 'w+')
        file.close()

    timer = time.time() + duration

    while time.time() < timer:
        data, addr = udp.udp_listen(ip_addr)

        if data is not None and addr[0] == ip_str:
            #print(addr, data)
            # print("New Reply")
            #print(data.decode("utf-8"), end="")

            with open(filepath, 'a', encoding='utf-8') as f:
                try:
                    f.write(data.decode("utf-8"))

                except Exception as e:

                    f.write("Failed to decode UDP string. " + str(e))

                    for x in range(len(data)):
                        f.write("data[" +  str(x) + "] = " + str(data[x]))


