# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Imports

# Built in modules
import logging


# Third party modules
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import numpy as np

# Custom modules


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Constants


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Dash Layout
# %% Must be called before the callback functions as the decorators need the layout to determine the inputs


# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Classes




# %% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Functions




def scatter(a_df, a_title, a_xaxis, a_color=None):
    """
    :arg
    """
    _sig_types = ["analogue", "digital", "pulsecounter"]

    #print("plotly_scatter, args {} {}".format(len(a_df), a_xaxis))

    fig = make_subplots(rows=3, cols=1)

    fig = fig.update_layout(
        title=a_title,
        xaxis_title="Time",
        yaxis_title="Digital Reading",
        legend_title="Signals",
        font=dict(
            family="Courier New, monospace",
            size=10,
            color="RebeccaPurple"
        ))

    for _column in a_df.columns:

        for _sig_type in _sig_types:

            if _sig_type in _column:
                fig = fig.add_trace(go.Scatter(x=a_df[a_xaxis], y=a_df[_column], name=_column), row=_sig_types.index(_sig_type) + 1, col=1)

    return fig

    # Check to see what
    if a_xaxis == "index":
        _ts_col = a_df.index

    else:
        TIME_COLUMNS = ['timestamp', 'Date']

        _ts_columns = list(set(TIME_COLUMNS).intersection(a_df.columns))

        print("_ts_columns: {}".format(_ts_columns))

        if len(_ts_columns) > 1:
            print("Error, more than 1 timestamp column.")

        _ts_col = _ts_columns[0]

    #_df = a_df.copy()

    ACCEPTED_COLUMN_TYPES = [np.float64, np.int64, float]

    _col_count = len(a_df.columns)
    _cols_loaded = 0
    _data_points = 0

    _axes = {"y1_min": 0, "y1_max": 0, "y2_min": 0, "y2_max": 0}

    # Create traces
    # fig = go.Figure()
    #fig = sp.make_subplots(rows=2, cols=1)
    print("plotly_scatter, initialise figure with 2 subplots. Parse over {} columns, {}".format(_col_count, a_df.columns))

    #fig = px.line(a_df, x=_ts_col, y=a_yaxis, color=a_color)

    #fig.update_layout({'color': main.CSS_COLOURS["background"]})
    print("plotly_scatter_fig, fig type({})".format(type(fig)))

    return fig