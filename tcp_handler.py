# -*- coding: utf-8 -*-
"""
Created on Wed Aug 15 15:27:58 2018

@author: Dave.Smith
"""
import socket
#import xml_builder as xml
import datetime
import xml.etree.ElementTree as ET


def send(hub, command):
    '''

    '''
    # Build and send a new ip address
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.settimeout(10)

    ip_str = (str(hub.ip[0]) + "." + str(hub.ip[1]) + "." +
              str(hub.ip[2]) + "." + str(hub.ip[3]))

    try:
        tcp.connect((ip_str, 4678))

    except Exception as e:
        print("failed connection", e)
        return False

    byte_array = command.encode('utf-8')
    tcp.send(byte_array)

    try:
        reply = tcp.recv(1000)

    except Exception as e:
        print(e)
        return

    print(reply)

    tcp.close()

    return reply


def send_bytearray(hub, command):
    '''

    '''
    print("Tx: {}".format(command))

    # Build and send a new ip address
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.settimeout(10)

    ip_str = (str(hub.ip[0]) + "." + str(hub.ip[1]) + "." +
              str(hub.ip[2]) + "." + str(hub.ip[3]))

    try:
        tcp.connect((ip_str, 4678))

    except Exception as e:
        print("failed connection", e)
        return None

    #byte_array = command.encode('utf-8')
    tcp.send(command)

    try:
        reply = tcp.recv(2000)

    except Exception as e:
        print(e)
        return None

    tcp.close()

    return reply


def stream_bytearray(hub, command, call_back=None, call_back_amount=1, call_back_kwargs={}):
    '''

    '''
    # Build and send a new ip address
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.settimeout(10)

    ip_str = (str(hub.ip[0]) + "." + str(hub.ip[1]) + "." +
              str(hub.ip[2]) + "." + str(hub.ip[3]))

    try:
        tcp.connect((ip_str, 4678))

    except Exception as e:
        print("failed connection", e)
        return False

    #byte_array = command.encode('utf-8')
    _reply = bytearray()
    tcp.send(command)
    last_update = 0
    while True:
        try:
            buffer = tcp.recv(10240)

            # logger.debug(buffer)
        except Exception as e:
            # print("ChunkDownload - recv data len: {} ".format(len(buffer)))
            print("ChunkDownload - recv Exception: {}".format(e))

            break

        _reply += buffer

        #Progress call back
        if call_back is not None:
            update_diff = len(_reply) - last_update 
            if update_diff > call_back_amount:
                # Do a callback
                try:
                    call_back(finished=False, data_in=len(_reply), **call_back_kwargs)
                except Exception as e:
                    print("Exception on downloda call back", e)
                    
                last_update = len(_reply)
            else:
                # nee bother
                pass

        if buffer.endswith("stream_end".encode("utf-8")):
            if call_back is not None:
                try:
                    call_back(finished=True, data_in=len(_reply), **call_back_kwargs)
                except Exception as e:
                    print("Exception on downloda call back", e)
            break

    tcp.close()

    return _reply

def setTime(hub):
    '''

    '''

    ip_str = (str(hub.ip[0]) + "." + str(hub.ip[1]) + "." +
              str(hub.ip[2]) + "." + str(hub.ip[3]))

    # Build and send a new ip address
    tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp.settimeout(30)

    try:
        tcp.connect((ip_str, 4678))
        print("Successful connection")

    except Exception as e:
        print("failed connection", e)

    xmlStatus = xml.XMLPkt()
    xmlStatus.tagRoot = "request"
    xmlStatus.tagName = "change"
    synctime_utc = datetime.datetime.utcnow()
    stime = synctime_utc.strftime("%d-%m-%Y %H:%M:%S")
    xmlStatus.params["time"] = stime

    etxml = xmlStatus.generateXMLElement()
    xmlString = ET.tostring(etxml)
    xmlString = xmlString.decode('utf-8')
    xmlString = xmlString + '\x0D\x0A'

    xml_byte_array = xmlString.encode('utf-8')

    tcp.send(xml_byte_array)
    tcp.close()